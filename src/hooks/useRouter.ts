import { useContext } from 'react'
import { RouterContext } from '../App'

const useRouter = () => {
  return useContext(RouterContext)
}

export default useRouter
