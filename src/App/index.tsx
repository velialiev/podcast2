import React, { useState } from 'react'
import '@vkontakte/vkui/dist/vkui.css'
import bridge from '@vkontakte/vk-bridge'
import { View, Root, Panel } from '@vkontakte/vkui'
import StartScreen from '../features/Podcast/StartScreen'
import PodcastCreationScreen from '../features/Podcast/PodcastCreationScreen'
import AudioEditingScreen from '../features/Podcast/AudioEditingScreen'
import MusicScreen from '../features/Podcast/MusicScreen'
import PodcastPreviewScreen from '../features/Podcast/PodcastPreviewScreen'
import DoneScreen from '../features/Podcast/DoneScreen'
import { Formik } from 'formik'
import * as Yup from 'yup'

export enum ViewId {
  Podcast = 'Podcast',
}

export enum PanelId {
  Start = 'Start',
  PodcastCreation = 'PodcastCreation',
  AudioEditing = 'AudioEditing',
  Music = 'Music',
  PodcastPreview = 'PodcastPreview',
  Done = 'Done',
  Modal = 'Modal',
}

export const RouterContext = React.createContext<RouterContextValue>({
  currentViewId: ViewId.Podcast,
  currentPanelId: PanelId.Start,
  goBack: () => {},
  goForward: () => {},
  history: [PanelId.Start],
})

type RouterContextValue = {
  currentViewId: ViewId
  currentPanelId: PanelId
  goBack: () => void
  goForward: (panelId: PanelId, data?: any) => void
  history: PanelId[]
  data?: any
  setData?: (data: any) => void
}

enum Accessibility {
  All = 'All',
  Me = 'Me',
}

export type InitialValues = {
  avatar: File | undefined
  podcastName: string
  description: string
  incorrectContent: boolean
  excludeEpisodeFromExport: boolean
  podcastTrailer: boolean
  accessibility: Accessibility
  audio: File | undefined
  duration: string
  timings: [{ id: number; description: string; time: string | number }]
}

const App = () => {
  const [currentViewId] = useState<ViewId>(ViewId.Podcast)
  const [currentPanelId, setCurrentPanelId] = useState<PanelId>(PanelId.Start)
  const [history, setHistory] = useState<PanelId[]>([PanelId.Start])
  const [routerData, setRouterData] = useState()

  const goBack = () => {
    const updatedHistory = [...history]
    updatedHistory.pop()
    const panelId = updatedHistory[updatedHistory.length - 1]

    if (panelId === PanelId.Start) {
      bridge.send('VKWebAppDisableSwipeBack')
    }

    setHistory(updatedHistory)
    setCurrentPanelId(panelId)
  }

  const goForward = (panelId: PanelId, data?: any) => {
    const updatedHistory = [...history]
    updatedHistory.push(panelId)

    if (currentPanelId === PanelId.Start) {
      bridge.send('VKWebAppEnableSwipeBack')
    }

    setHistory(updatedHistory)
    setCurrentPanelId(panelId)
    setRouterData(data)
  }

  const initialValues: InitialValues = {
    avatar: undefined,
    podcastName: '',
    description: '',
    incorrectContent: false,
    excludeEpisodeFromExport: false,
    podcastTrailer: false,
    accessibility: Accessibility.All,
    audio: undefined,
    duration: '00:00',
    timings: [] as any,
  }

  const validationSchema = Yup.object().shape({
    podcastName: Yup.string().required('This field is required'),
    description: Yup.string().required('This field is required'),
  })

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={() => console.log()}
      validationSchema={validationSchema}
    >
      <RouterContext.Provider
        value={{
          currentViewId,
          currentPanelId,
          data: routerData,
          setData: setRouterData,
          goBack,
          goForward,
          history,
        }}
      >
        <Root activeView={currentViewId}>
          <View id={ViewId.Podcast} activePanel={currentPanelId}>
            <Panel id={PanelId.Start}>
              <StartScreen />
            </Panel>

            <Panel id={PanelId.PodcastCreation}>
              <PodcastCreationScreen />
            </Panel>

            <Panel id={PanelId.AudioEditing}>
              <AudioEditingScreen />
            </Panel>

            <Panel id={PanelId.Music}>
              <MusicScreen />
            </Panel>

            <Panel id={PanelId.PodcastPreview}>
              <PodcastPreviewScreen />
            </Panel>

            <Panel id={PanelId.Done}>
              <DoneScreen />
            </Panel>

            <Panel id={PanelId.Modal}>{routerData?.renderContent?.()}</Panel>
          </View>
        </Root>
      </RouterContext.Provider>
    </Formik>
  )
}

export default App
