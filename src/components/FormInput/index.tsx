import React, { FC } from 'react'
import { InputProps } from '@vkontakte/vkui/dist/components/Input/Input'
import { Input } from '@vkontakte/vkui'
import { useField } from 'formik'

const FormInput: FC<Props> = ({ name, type, ...props }) => {
  const [field] = useField(name)

  return (
    <Input
      {...props}
      value={field.value}
      onChange={field.onChange(name)}
      onBlur={field.onBlur(name)}
      type={type || 'text'}
    />
  )
}

interface Props extends InputProps {
  name: string
}

export enum ValidationStatus {
  Error = 'error',
  Valid = 'valid',
  Default = 'default',
}

export default FormInput
