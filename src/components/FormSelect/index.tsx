import React, { FC, PropsWithChildren, ReactElement } from 'react'
import Select, { SelectProps } from '@vkontakte/vkui/dist/components/Select/Select'
import { useField } from 'formik'

const FormSelect = <T, >({
  name,
  options,
  resolveId,
  resolveContent,
  ...props
}: PropsWithChildren<Props<T>>) => {
  const [field] = useField(name)
  return (
    <Select
      {...props}
      value={field.value}
      onChange={field.onChange(name)}
      onBlur={field.onBlur(name)}
    >
      {options.map((option) => {
        const id = resolveId(option)
        const content = resolveContent(option)

        return (
          <option key={id} value={id}>
            {content}
          </option>
        )
      })}
    </Select>
  )
}

interface Props<T> extends SelectProps {
  name: string
  resolveId: (option: T) => string | number
  resolveContent: (option: T) => ReactElement | string
  options: T[]
}

export default FormSelect
