import React, {
  FC, useEffect, useRef, useState, 
} from 'react'
import {
  Avatar, Div, getClassName, Text, usePlatform, 
} from '@vkontakte/vkui'
import { Icon56GalleryOutline } from '@vkontakte/icons'
import './index.scss'
import { useField } from 'formik'
import cn from 'classnames'

const FileUploader: FC<Props> = ({ name }) => {
  const platform = usePlatform()
  const fileInputRef = useRef<HTMLInputElement>(null)
  const [field, , helper] = useField(name)
  const [preview, setPreview] = useState('')
  const fileUploaderClassName = cn(getClassName('FileUploader', platform), {
    'FileUploader--preview': preview,
  })

  const onFileInputClick = () => {
    if (!fileInputRef.current) {
      return
    }

    fileInputRef.current.click()
  }

  const onFileUpload = () => {
    if (!fileInputRef.current || !fileInputRef.current.files) {
      return
    }

    helper.setValue(fileInputRef.current.files[0])
  }

  useEffect(() => {
    if (!field.value) {
      setPreview('')
      return
    }

    setPreview(URL.createObjectURL(field.value))
  }, [field.value])

  return (
    <div onClick={onFileInputClick}>
      {!field.value ? (
        <Avatar mode="app" size={64}>
          <Icon56GalleryOutline fill="#3F8AE0" width={25} height={25} />
        </Avatar>
      ) : (
        <Avatar mode="app" size={64}>
          <img
            src={URL.createObjectURL(field.value)}
            alt=""
            style={{
              width: 64,
              height: 64,
              borderRadius: 10,
              objectFit: 'cover',
            }}
          />
        </Avatar>
      )}
      <input
        accept="image/*"
        type="file"
        style={{ display: 'none' }}
        ref={fileInputRef}
        onChange={onFileUpload}
      />
    </div>
  )
}

interface Props {
  name: string
}

export default FileUploader
