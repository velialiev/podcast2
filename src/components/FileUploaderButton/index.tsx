import React, { FC } from 'react'
import { File } from '@vkontakte/vkui'
import { useField } from 'formik'
import { FileProps } from '@vkontakte/vkui/dist/components/File/File'

const FileUploaderButton: FC<Props> = ({ name, ...props }) => {
  const [, , helper] = useField(name)
  return (
    <File
      {...props}
      mode="outline"
      controlSize="l"
      onChange={(e) => {
        const file = e.target.files?.[0]

        helper.setValue(file)

        if (props.onChange) {
          props.onChange(e)
        }
      }}
    >
      Загрузить файл
    </File>
  )
}

interface Props extends FileProps {
  name: string
}

export default FileUploaderButton
