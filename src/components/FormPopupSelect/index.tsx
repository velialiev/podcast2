import React, {
  PropsWithChildren, ReactText, useEffect, useMemo, 
} from 'react'
import {
  Banner, Cell, Div, Group, List, Text, 
} from '@vkontakte/vkui'
import { useField } from 'formik'
import useRouter from '../../hooks/useRouter'
import { PanelId } from '../../App'
import './index.scss'
import Header from '../Header'
import Icon24DoneOutline from '@vkontakte/icons/dist/24/done_outline'

const FormPopupSelect = <T, >({
  label,
  name,
  data,
  resolveId,
  resolveTitle,
  resolveDescription,
}: PropsWithChildren<Props<T>>) => {
  const router = useRouter()
  const [field, , helper] = useField(name)

  const closeModal = (id: ReactText) => {
    router.goForward(router.history[router.history.length - 1], { id })
  }

  const openSelectModal = () => {
    router.goForward(PanelId.Modal, {
      renderContent: renderModal,
    })
  }

  const renderModal = () => (
    <>
      <Header
        onBackButtonClick={() => {
          if (router.setData) {
            router.setData({ id: field.value })
          }
        }}
      >
        {label}
      </Header>
      <Group>
        <List>
          {data.map((item) => {
            const id = resolveId(item)

            return (
              <Cell
                asideContent={id === field.value && <Icon24DoneOutline fill="#3F8AE0" />}
                key={id}
                onClick={() => closeModal(id)}
              >
                {resolveTitle(item)}
              </Cell>
            )
          })}
        </List>
      </Group>
    </>
  )

  useEffect(() => {
    if (router.data?.id) {
      helper.setValue(router.data.id)
    }
  }, [router.data?.id])

  const values = useMemo(() => {
    const currentItem = data.find((item) => resolveId(item) === field.value)

    if (!currentItem) {
      return {}
    }

    return {
      title: resolveTitle(currentItem),
      description: resolveDescription(currentItem),
    }
  }, [field.value, data])

  return (
    <Div>
      <Banner
        className="FormPopupSelect"
        header={label}
        subheader={values.title}
        asideMode="expand"
        onClick={openSelectModal}
      />
      <Text className="Hint" weight="regular">
        {values.description}
      </Text>
    </Div>
  )
}

interface Props<T> {
  label: string
  name: string
  data: T[]
  resolveId: (item: T) => string | number
  resolveTitle: (item: T) => string
  resolveDescription: (item: T) => string
}

export default FormPopupSelect
