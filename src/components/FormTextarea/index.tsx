import React, { FC } from 'react'
import { InputProps } from '@vkontakte/vkui/dist/components/Input/Input'
import { Input, Textarea } from '@vkontakte/vkui'
import { useField } from 'formik'
import { TextareaProps } from '@vkontakte/vkui/dist/components/Textarea/Textarea'

const FormTextarea: FC<Props> = ({ name, ...props }) => {
  const [field] = useField(name)

  return (
    <Textarea
      {...props}
      value={field.value}
      onChange={field.onChange(name)}
      onBlur={field.onBlur(name)}
    />
  )
}

interface Props extends TextareaProps {
  name: string
}

export default FormTextarea
