const applyQueryParams = (url: string, params: Record<string, string>) => {
  return Object.entries(params).reduce((acc, [key, value], idx) => {
    if (idx === 0) {
      return `${acc}${key}=${value}`
    }

    return `${acc}&${key}=${value}`
  }, `${url}?`)
}

export default applyQueryParams
