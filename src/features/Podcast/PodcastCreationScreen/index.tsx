import React, { ChangeEvent, useRef } from 'react'
import Header from '../../../components/Header'
import { useFormikContext } from 'formik'
import FormInput from '../../../components/FormInput'
import {
  Avatar,
  Button,
  Checkbox,
  Div,
  FormLayout,
  FormLayoutGroup,
  getClassName,
  Placeholder,
  Separator,
  SimpleCell,
  Text,
  usePlatform,
} from '@vkontakte/vkui'
import FormTextarea from '../../../components/FormTextarea'
import { InitialValues, PanelId } from '../../../App'
import * as Yup from 'yup'
import useRouter from '../../../hooks/useRouter'
import FileUploader from '../../../components/FileUploader'
import FormPopupSelect from '../../../components/FormPopupSelect'
import './index.scss'
import Icon28PodcastOutline from '@vkontakte/icons/dist/28/podcast_outline'
import FileUploaderButton from '../../../components/FileUploaderButton'
import moment from 'moment'

enum Accessibility {
  All = 'All',
  Me = 'Me',
}

const validationSchema = Yup.object().shape({
  podcastName: Yup.string().required('This field is required'),
  description: Yup.string().required('This field is required'),
})

const mockedAccessibility = [
  {
    id: Accessibility.All,
    title: 'Всем пользователям',
    description: 'При публикации записи с эпизодом, он становится доступным для всех пользователей',
  },
  {
    id: Accessibility.Me,
    title: 'Только мне',
    description: 'При публикации записи с эпизодом, он становится только вам',
  },
]

const PodcastCreationScreen = () => {
  const router = useRouter()
  const platform = usePlatform()
  const {
    handleSubmit, values, isValid, touched, setFieldValue, 
  } = useFormikContext<
    InitialValues
  >()

  const labelClassname = getClassName('label', platform)

  const audioRef = useRef<HTMLAudioElement>(document.createElement('audio'))

  const isSomeFieldTouched = (formTouched: Record<string, boolean | undefined>) => {
    return Object.values(formTouched).some((isTouched) => isTouched)
  }

  const onAudioUpload = (e: ChangeEvent<HTMLInputElement>) => {
    const audio = e.target.files?.[0]
    const { current } = audioRef

    if (!audio) {
      return
    }

    current.src = URL.createObjectURL(audio)
    current.onloadedmetadata = () => {
      const momentDuration = moment.duration(current.duration, 'seconds')

      let time = ''
      const hours = momentDuration.hours()

      if (hours > 0) {
        time = `${hours}:`
      }

      time = `${time + momentDuration.minutes()}:${momentDuration.seconds()}`

      setFieldValue('duration', time)
    }
  }

  return (
    <>
      <Header>Новый подкаст</Header>
      <FormLayout>
        <SimpleCell className="PodcastNameAndAvatar" before={<FileUploader name="avatar" />}>
          <Text weight="regular" className={labelClassname}>
            Название
          </Text>
          <FormInput
            top="Название"
            name="podcastName"
            placeholder="Введите название подкаста"
            title="Название"
          />
        </SimpleCell>

        <FormLayoutGroup top="Описание">
          <FormTextarea
            name="description"
            placeholder="Введите описание подкаста"
            title="Описание"
          />
        </FormLayoutGroup>

        {!values.audio ? (
          <Placeholder
            className="PodcastUploadPlaceholder"
            header="Загрузите ваш подкаст"
            action={(
              <FileUploaderButton
                accept=".mp3,audio/*"
                name="audio"
                mode="outline"
                controlSize="l"
                onChange={onAudioUpload}
              >
                Загрузить файл
              </FileUploaderButton>
            )}
          >
            Выберите готовый аудиофайл из вашего телефона и добавьте его
          </Placeholder>
        ) : (
          <>
            <SimpleCell
              before={(
                <Avatar mode="image">
                  <Icon28PodcastOutline />
                </Avatar>
              )}
              after={(
                <Text weight="regular" style={{ color: 'var(--text_secondary)' }}>
                  {values.duration}
                </Text>
              )}
            >
              {values.audio.name}
            </SimpleCell>
            <Div>
              <Text weight="regular" className="Hint">
                Вы можете добавить таймкоды и скорректировать подкаст в режиме редактирования
              </Text>
            </Div>
            <Button size="xl" mode="outline" onClick={() => router.goForward(PanelId.AudioEditing)}>
              Редактировать аудиозапись
            </Button>
          </>
        )}

        <Separator wide />
        <div>
          <Checkbox
            name="incorrectContent"
            checked={values.incorrectContent}
            onClick={() => setFieldValue('incorrectContent', !values.incorrectContent)}
          >
            Ненормативный контент
          </Checkbox>

          <Checkbox
            name="excludeEpisodeFromExport"
            checked={values.excludeEpisodeFromExport}
            onClick={() => setFieldValue('excludeEpisodeFromExport', !values.excludeEpisodeFromExport)}
          >
            Исключить эпизод из экспорта
          </Checkbox>

          <Checkbox
            name="podcastTrailer"
            checked={values.podcastTrailer}
            onClick={() => setFieldValue('podcastTrailer', !values.podcastTrailer)}
          >
            Трейлер подкаста
          </Checkbox>
        </div>

        <FormPopupSelect
          label="Кому доступен данный подкаст"
          name="accessibility"
          data={mockedAccessibility}
          resolveId={(item) => item.id}
          resolveTitle={(item) => item.title}
          resolveDescription={(item) => item.description}
        />
        <Button
          disabled={!isSomeFieldTouched(touched as any) || !isValid || !values.audio?.size}
          size="xl"
          onClick={() => router.goForward(PanelId.PodcastPreview)}
        >
          Далее
        </Button>
      </FormLayout>
    </>
  )
}

export default PodcastCreationScreen
