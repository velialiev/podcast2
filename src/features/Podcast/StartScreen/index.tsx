import React from 'react'
import Header from '../../../components/Header'
import { Button, Group, Placeholder } from '@vkontakte/vkui'
import Icon56AddCircleOutline from '@vkontakte/icons/dist/56/add_circle_outline'
import useRouter from '../../../hooks/useRouter'
import { PanelId } from '../../../App'

const StartScreen = () => {
  const router = useRouter()

  return (
    <>
      <Header>Подкасты</Header>
      <Group>
        <Placeholder
          stretched
          icon={<Icon56AddCircleOutline />}
          header="Добавьте первый подкаст"
          action={(
            <Button onClick={() => router.goForward(PanelId.PodcastCreation)} size="l">
              Добавить подкаст
            </Button>
          )}
        >
          Добавляйте, редактируйте и делитесь подкастами вашего сообщества.
        </Placeholder>
      </Group>
    </>
  )
}

export default StartScreen
