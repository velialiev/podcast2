import React, {
  ReactElement,
  useEffect,
  useMemo,
  useRef,
  useState,
  MouseEvent,
  RefObject, FC,
} from 'react'
import Header from '../../../components/Header'
import './index.scss'
import moment from 'moment'
import {
  FieldArray, FieldArrayRenderProps, setIn, useFormikContext,
} from 'formik'
import { InitialValues, PanelId } from '../../../App'
import {
  Button, Div, Title, HorizontalScroll, Group,
} from '@vkontakte/vkui'
import { Icon24Add, Icon28RemoveCircleOutline } from '@vkontakte/icons'
import FormInput from '../../../components/FormInput'
import scrubberImage from '../../../assets/images/scrubber.png'
import trimmerImage from '../../../assets/images/trimmer.svg'
import Icon24Play from '@vkontakte/icons/dist/24/play'
import Icon24ArrowUturnLeftOutline from '@vkontakte/icons/dist/24/arrow_uturn_left_outline'
import Icon24Music from '@vkontakte/icons/dist/24/music'
import { ReactComponent as ChartIcon } from '../../../assets/icons/chart.svg'
import { ReactComponent as ReversedChartIcon } from '../../../assets/icons/reversedChart.svg'
import { ReactComponent as ScissorsIcon } from '../../../assets/icons/scissors.svg'
import Icon24Pause from '@vkontakte/icons/dist/24/pause'
import useRouter from '../../../hooks/useRouter'
// @ts-ignore
import mozart from '../../../assets/music/mozart.mp3'

const AUDIO_TRACK_HEIGHT = 100
const DRAW_LINES = 150
const LINE_WIDTH = 5
const GAP = 3
const LINE_HEIGHT_MULTIPLIER = 1
const MIN_LINE_HEIGHT = 20

const generateRandomId = () => Math.floor(Math.random() * 10 ** 10)

let isCropped = false
let fadeIn = false
let fadeOut = false
let backgroundMusic = false

const AudioEditingScreen = () => {
  const router = useRouter()
  const { values } = useFormikContext<InitialValues>()
  const [currentAudioBuffer, setCurrentAudioBuffer] = useState()
  const audioTrackRef = useRef<HTMLDivElement>(null)
  const audioRef = useRef<HTMLAudioElement>(document.createElement('audio'))
  const backgroundAudioRef = useRef<HTMLAudioElement>(document.createElement('audio'))
  const [scrubberPosition, setScrubberPosition] = useState(0)
  const [isPlaying, setIsPlaying] = useState(false)
  const [trimmerLeftPosition, setTrimmerLeftPosition] = useState(0)
  const trimmerLeftRef = useRef<HTMLImageElement>(null)
  const trimmerRightRef = useRef<HTMLImageElement>(null)
  const scrubberRef = useRef<HTMLImageElement>(null)
  const trimmerRef = useRef<HTMLDivElement>(null)
  const [fadeInIconActive, setFadeInIconActive] = useState(fadeIn)
  const [fadeOutIconActive, setFadeOutIconActive] = useState(fadeOut)
  const [croppedIconActive, setCroppedIconActive] = useState(isCropped)
  const { useBackgroundMusic } = router.data || {}
  const [musicIconActive, setMusicIconActive] = useState(useBackgroundMusic)
  const [currentInterval, setCurrentInterval] = useState()

  const resolveInitialTrimmerRightPosition = () => {
    if (!audioTrackRef.current) {
      return 200
    }

    return Math.min(200, audioTrackRef.current.scrollWidth)
  }
  const [trimmerRightPosition, setTrimmerRightPosition] = useState(
    resolveInitialTrimmerRightPosition(),
  )

  const play = () => {
    setIsPlaying(true)

    if (musicIconActive) {
      if (!backgroundAudioRef.current.src) {
        backgroundAudioRef.current.src = mozart
      }

      backgroundAudioRef.current.volume = 0.6
      backgroundAudioRef.current.play()
    }

    audioRef.current.play().then(() => {
      const interval = setInterval(() => {
        if (!scrubberRef.current || !trimmerRef.current || !audioTrackRef.current) {
          return
        }

        const width = +trimmerRef.current.style.width.split('px')[0]
        const left = +trimmerRef.current.style.left.split('px')[0]
        const right = width + left

        audioRef.current.volume = 1

        if (fadeIn) {
          const startPosition = isCropped
            ? (left / audioTrackRef.current.scrollWidth)
            * audioRef.current.duration
            : 0

          if (audioRef.current.currentTime - startPosition < 5 || !fadeOut) {
            const volume = Math.max(
              Math.min((audioRef.current.currentTime - startPosition) * 0.2, 1),
              0,
            )
            audioRef.current.volume = volume
          }
        }

        if (fadeOut) {
          const endPosition = isCropped
            ? (right / audioTrackRef.current.scrollWidth)
            * audioRef.current.duration
            : audioRef.current.duration

          if (endPosition - audioRef.current.currentTime < 5 || !fadeIn) {
            const volume = Math.max(
              Math.min((endPosition - audioRef.current.currentTime) * 0.2, 1), 0,
            )
            audioRef.current.volume = volume
          }
        }

        setScrubberPosition(audioRef.current.currentTime)

        if (isCropped) {
          if (scrubberRef.current.offsetLeft >= right) {
            clearInterval(interval)
            pause()
          }
        }
      }, 50)

      setCurrentInterval(interval)

      audioRef.current.onpause = () => {
        clearInterval(interval)
        pause()
      }

      audioRef.current.onended = () => {
        clearInterval(interval)
        pause()
      }
    })
  }

  const pause = () => {
    setIsPlaying(false)
    audioRef.current.pause()
    backgroundAudioRef.current.pause()
  }

  const rewind = (x: number) => {
    if (!audioTrackRef.current || !trimmerRef.current) {
      return
    }

    const width = +trimmerRef.current.style.width.split('px')[0]
    const left = +trimmerRef.current.style.left.split('px')[0]
    const right = left + width

    const processedX = isCropped ? Math.max(Math.min(x, right), left) : x

    const time = (processedX / audioTrackRef.current.scrollWidth) * audioRef.current.duration

    if (audioRef.current.ended) {
      play()
      audioRef.current.currentTime = time
      return
    }

    audioRef.current.currentTime = time
    setScrubberPosition(time)
  }

  const calculateScrubberXCoord = () => {
    if (!audioTrackRef.current || !audioRef.current) {
      return 0
    }

    return Math.min(
      (audioTrackRef.current.scrollWidth / audioRef.current.duration)
      * audioRef.current.currentTime,
      audioTrackRef.current.scrollWidth,
      DRAW_LINES * (LINE_WIDTH + GAP),
    )
  }

  const markup = useMemo(() => {
    if (!currentAudioBuffer) {
      return null
    }

    const drawLines = DRAW_LINES
    const leftChannel = currentAudioBuffer.getChannelData(0) // Float32Array describing left channel
    const totalLength = leftChannel.length
    const eachBlock = Math.floor(totalLength / drawLines)

    const timelineMarkup: ReactElement[] = []

    const trackMarkup = Array(drawLines)
      .fill(null)
      .map((_, i, arr) => {
        const resolveTimeLineHeight = () => {
          if (i === 0 || i === arr.length - 1) {
            return 0
          }

          if (i % 10 === 0) {
            return 21
          }

          if (i % 5 === 0) {
            return 16
          }

          return 7
        }

        let time = ''

        if (i % 10 === 0) {
          const { duration } = audioRef.current
          const momentDuration = moment.duration((duration / DRAW_LINES) * i, 'seconds')

          let hours: number | string = momentDuration.hours()
          let minutes: number | string = momentDuration.minutes()
          let seconds: number | string = momentDuration.seconds()

          if (hours < 10) {
            hours = `0${hours}`
          }

          if (minutes < 10) {
            minutes = `0${minutes}`
          }

          if (seconds < 10) {
            seconds = `0${seconds}`
          }

          if (hours > 0) {
            time = `${hours}:`
          }

          time = `${time + minutes}:${seconds}`
        }

        timelineMarkup.push(
          // eslint-disable-next-line react/no-array-index-key
          <div key={i} style={{ height: resolveTimeLineHeight() }} className="Timeline__line">
            {i % 10 === 0 && i !== 0 && <span>{time}</span>}
          </div>,
        )

        const audioBuffKey = Math.floor(eachBlock * i)
        const height = Math.max(
          leftChannel[audioBuffKey]
          * (audioTrackRef.current?.offsetHeight || AUDIO_TRACK_HEIGHT)
          * LINE_HEIGHT_MULTIPLIER,
          Math.floor(MIN_LINE_HEIGHT * Math.random()),
          MIN_LINE_HEIGHT * 0.4,
        )

        // eslint-disable-next-line react/no-array-index-key
        return <div key={i} style={{ height }} className="Track__line"/>
      })

    return {
      trackMarkup,
      timelineMarkup,
    }
  }, [currentAudioBuffer])

  useEffect(() => {
    if (!values.audio) {
      return
    }

    const audioContext = new AudioContext()
    const fileReader = new FileReader()

    fileReader.onloadend = () => {
      const arrayBuffer = fileReader.result as ArrayBuffer

      audioContext.decodeAudioData(arrayBuffer, (audioBuffer: AudioBuffer) => {
        setCurrentAudioBuffer(audioBuffer)
      })
    }

    fileReader.readAsArrayBuffer(values.audio)
  }, [values.audio])

  useEffect(() => {
    if (!values.audio) {
      return
    }

    const { current } = audioRef

    current.src = URL.createObjectURL(values.audio)
  }, [values.audio])

  const addDraggability = (ref: RefObject<any>, setPosition: (x: number) => void) => {
    const { current } = ref

    if (!current) {
      return
    }

    let isMouseDown = false

    const scrollerRef = document.querySelector('.HorizontalScroll__in')

    current.addEventListener('mousedown', () => {
      isMouseDown = true
    })

    window.addEventListener('mouseup', () => {
      isMouseDown = false
    })

    window.addEventListener('mousemove', (evt) => {
      if (!isMouseDown || !scrollerRef) {
        return
      }

      setPosition(evt.clientX + scrollerRef.scrollLeft)
    })

    current.addEventListener('touchstart', () => {
      isMouseDown = true

      if (!scrollerRef) {
        return
      }

      scrollerRef.classList.add('NonScrollable')
    })

    window.addEventListener('touchend', () => {
      isMouseDown = false

      if (!scrollerRef) {
        return
      }

      scrollerRef.classList.remove('NonScrollable')
    })

    window.addEventListener('touchmove', (evt) => {
      if (!isMouseDown || !scrollerRef) {
        return
      }

      setPosition(evt.touches[0].clientX + scrollerRef.scrollLeft)
    })
  }

  useEffect(() => {
    addDraggability(trimmerLeftRef, (x) => {
      if (!audioTrackRef.current || !trimmerRef.current) {
        setTrimmerRightPosition(0)
        return
      }

      const [width] = trimmerRef.current.style.width.split('px')
      const [left] = trimmerRef.current.style.left.split('px')
      console.log(width)
      console.log(left)

      setTrimmerLeftPosition(Math.max(Math.min(x, +width + +left - 30), 0))
    })

    addDraggability(trimmerRightRef, (x) => {
      if (!audioTrackRef.current || !trimmerRef.current) {
        setTrimmerRightPosition(0)
        return
      }

      const [left] = trimmerRef.current.style.left.split('px')

      setTrimmerRightPosition(Math.min(Math.max(x, +left + 30), audioTrackRef.current.scrollWidth))
    })
    addDraggability(scrubberRef, (x) => {
      if (!audioTrackRef.current || !trimmerRef.current) {
        return
      }

      rewind(x)
    })
  }, [])

  return (
    <>
      <Header>Редактирование</Header>

      <Group>
        <Div>
          <HorizontalScroll>
            <div style={{ width: `${DRAW_LINES * (LINE_WIDTH + GAP)}px` }} className="Player">
              <div className="Timeline">{markup?.timelineMarkup}</div>
              <div
                ref={audioTrackRef}
                className="Track"
                onClick={(e) => {
                  const scrollerRef = document.querySelector('.HorizontalScroll__in')

                  if (!scrollerRef) {
                    return
                  }

                  rewind(e.clientX + scrollerRef.scrollLeft)
                }}
              >
                {markup?.trackMarkup}
                <img
                  style={{
                    left: calculateScrubberXCoord(),
                  }}
                  className="Track__scrubber"
                  src={scrubberImage}
                  alt=""
                  ref={scrubberRef}
                />

                <div
                  ref={trimmerRef}
                  className="Trimmer"
                  style={{
                    left: trimmerLeftPosition,
                    width: trimmerRightPosition - trimmerLeftPosition,
                  }}
                >
                  {/* eslint-disable-next-line jsx-a11y/no-noninteractive-element-interactions */}
                  <img
                    onClick={(e) => e.stopPropagation()}
                    alt=""
                    ref={trimmerLeftRef}
                    src={trimmerImage}
                  />
                  {/* eslint-disable-next-line jsx-a11y/no-noninteractive-element-interactions */}
                  <img
                    onClick={(e) => e.stopPropagation()}
                    alt=""
                    ref={trimmerRightRef}
                    src={trimmerImage}
                  />
                </div>
              </div>
            </div>
          </HorizontalScroll>

          <div className="Actions">
            <div className="Actions__group">
              <Button className="Actions__button" onClick={isPlaying ? pause : play}>
                {!isPlaying ? <Icon24Play/> : <Icon24Pause/>}
              </Button>
            </div>
            <div className="Actions__group">
              <Button
                className="Actions__button"
                style={{ backgroundColor: croppedIconActive ? '#3F8AE0' : '#F2F3F5' }}
                onClick={() => {
                  isCropped = !isCropped
                  setCroppedIconActive(isCropped)

                  if (!trimmerRef.current) {
                    return
                  }

                  const left = +trimmerRef.current.style.left.split('px')[0]

                  rewind(left)
                }}
              >
                <ScissorsIcon
                  className={`Action ${croppedIconActive ? 'active' : ''}`}
                />
              </Button>
              <Button
                className="Actions__button"
                style={{ backgroundColor: '#F2F3F5' }}
              >
                <Icon24ArrowUturnLeftOutline fill="#3F8AE0"/>
              </Button>
            </div>
            <div className="Actions__group">
              <Button
                className="Actions__button"
                style={{ backgroundColor: musicIconActive ? '#3F8AE0' : '#F2F3F5' }}
                onClick={() => {
                  if (musicIconActive) {
                    setMusicIconActive(false)
                    backgroundMusic = false
                    backgroundAudioRef.current.pause()
                    return
                  }

                  pause()
                  clearInterval(currentInterval)
                  router.goForward(PanelId.Music)
                }}
              >
                <Icon24Music
                  className={`Action filled ${musicIconActive ? 'active' : ''}`}
                />
              </Button>
              <Button
                onClick={() => {
                  fadeIn = !fadeIn
                  setFadeInIconActive(fadeIn)
                }}
                className="Actions__button"
                style={{ backgroundColor: fadeInIconActive ? '#3F8AE0' : '#F2F3F5' }}
              >
                <ChartIcon
                  className={`Action ${fadeInIconActive ? 'active' : ''}`}
                />
              </Button>
              <Button
                className="Actions__button"
                style={{ backgroundColor: fadeOutIconActive ? '#3F8AE0' : '#F2F3F5' }}
                onClick={() => {
                  fadeOut = !fadeOut
                  setFadeOutIconActive(fadeOut)
                }}
              >
                <ReversedChartIcon
                  className={`Action ${fadeOutIconActive ? 'active' : ''}`}
                />
              </Button>
            </div>
          </div>
        </Div>
      </Group>

      <Div>
        <Title level="3" weight="semibold" style={{ color: '#818C99', marginBottom: 22 }}>
          Таймкоды
        </Title>

        <FieldArray name="timings">
          {(fieldArrayProps: FieldArrayRenderProps) => (
            <div>
              {values.timings.map((timeCode, index) => {
                return (
                  <div
                    key={timeCode.id}
                    style={{
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      marginBottom: 22,
                    }}
                  >
                    <Icon28RemoveCircleOutline
                      fill="#E64646"
                      width={24}
                      height={24}
                      onClick={() => fieldArrayProps.remove(index)}
                    />

                    <div
                      style={{
                        width: '56%',
                      }}
                    >
                      <FormInput placeholder="Описание" name={`timings[${index}].description`}/>
                    </div>

                    <div
                      style={{
                        width: '30%',
                      }}
                    >
                      <FormInput
                        placeholder="Время"
                        name={`timings[${index}].time`}
                        style={{
                          textAlign: 'center',
                        }}
                        type="time"
                      />
                    </div>
                  </div>
                )
              })}
              <div onClick={() => fieldArrayProps.push({ id: generateRandomId() })}>
                <Title
                  level="3"
                  weight="medium"
                  style={{
                    fontSize: 15,
                    color: '#4986CC',
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginTop: 22,
                  }}
                >
                  <Icon24Add style={{ marginRight: 5 }}/>
                  Добавить таймкоды
                </Title>
              </div>
            </div>
          )}
        </FieldArray>
      </Div>
    </>
  )
}

export default AudioEditingScreen
