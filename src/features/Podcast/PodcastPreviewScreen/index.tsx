import React from 'react'
import Header from '../../../components/Header'
import {
  Avatar, Button, Div, FixedLayout, Separator, SimpleCell, Title, 
} from '@vkontakte/vkui'
import { Icon56GalleryOutline } from '@vkontakte/icons'
import { useFormikContext } from 'formik'
import { InitialValues, PanelId } from '../../../App'
import useRouter from '../../../hooks/useRouter'

const descriptions = 'Подкаст, который рассказывает про то, как много в мире прекрасных вещей, которые можно совершить, а также сколько людей, которые могут помочь вам в реализации ваших заветных мечт.'

const PodcastPreviewScreen = () => {
  const router = useRouter()

  const { values } = useFormikContext<InitialValues>()
  return (
    <>
      <Header>Новый подкаст</Header>

      <SimpleCell
        before={
          values.avatar ? (
            <Avatar mode="app" size={64}>
              <img
                src={values.avatar && URL.createObjectURL(values.avatar)}
                alt=""
                style={{
                  width: 64,
                  height: 64,
                  borderRadius: 10,
                  objectFit: 'cover',
                }}
              />
            </Avatar>
          ) : (
            <Avatar mode="app" size={64}>
              <Icon56GalleryOutline fill="#3F8AE0" width={25} height={25} />
            </Avatar>
          )
        }
      >
        <Div>
          <Title level="3" weight="semibold">
            {values.podcastName}
          </Title>
          <Title level="3" weight="regular" style={{ fontSize: 12, color: '#4986CC' }}>
            Papa dogs
          </Title>
          <Title level="3" weight="semibold" style={{ fontSize: 12, color: '#818C99' }}>
            Длительность {values.duration}
          </Title>
        </Div>
      </SimpleCell>

      <Div>
        <Title level="3" weight="semibold" style={{ marginBottom: 10 }}>
          Описание
        </Title>
        <Title level="3" weight="medium" style={{ marginBottom: 10 }}>
          {values.description}
        </Title>
      </Div>
      <Separator wide />
      <Div style={{ paddingBottom: 100 }}>
        {values.timings.length ? (
          <>
            <Title level="3" weight="semibold" style={{ marginBottom: 10 }}>
              Содержание
            </Title>
            {values.timings
              .filter((timeCode) => timeCode.description && timeCode.time)
              .map((timeCode) => {
                return (
                  <div key={timeCode.id} style={{ display: 'flex', flexDirection: 'row' }}>
                    <Title
                      level="3"
                      weight="medium"
                      style={{ marginBottom: 15, color: '#4986CC', marginRight: 5 }}
                    >
                      {timeCode.time}
                    </Title>
                    <Title level="3" weight="medium" style={{ marginBottom: 15, color: 'black' }}>
                      {' '}
                      — {timeCode.description}
                    </Title>
                  </div>
                )
              })}
          </>
        ) : null}
        <FixedLayout vertical="bottom">
          <Div>
            <Button size="xl" onClick={() => router.goForward(PanelId.Done)}>
              Опубликовать подкаст
            </Button>
          </Div>
        </FixedLayout>
      </Div>
    </>
  )
}

export default PodcastPreviewScreen
