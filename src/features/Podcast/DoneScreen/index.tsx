import React from 'react'
import Header from '../../../components/Header'
import { Button, Group, Placeholder } from '@vkontakte/vkui'
import Icon56CheckCircleOutline from '@vkontakte/icons/dist/56/check_circle_outline'

const DoneScreen = () => {
  return (
    <>
      <Header>Подкасты</Header>
      <Group>
        <Placeholder
          stretched
          icon={<Icon56CheckCircleOutline />}
          header="Подкаст добавлен"
          action={<Button size="l">Поделиться подкастом</Button>}
        >
          Раскажите своим подписчикам о новом подкасте, чтобы получить больше слушателей.
        </Placeholder>
      </Group>
    </>
  )
}

export default DoneScreen
