import React from 'react'
import Header from '../../../components/Header'
import { Avatar, List, Search, SimpleCell, } from '@vkontakte/vkui'
import { Icon24MoreHorizontal } from '@vkontakte/icons'
import mocart from '../../../assets/images/mocart.jpg'
import useRouter from '../../../hooks/useRouter'
import { PanelId } from '../../../App'

const thematics = [
  { id: 3201, name: 'Аренда автомобилей' },
  { id: 3273, name: 'Автотовары' },
  { id: 3205, name: 'Автосалон' },
  { id: 3282, name: 'Автосервис' },
  { id: 3283, name: 'Услуги для автовладельцев' },
  { id: 3284, name: 'Велосипеды' },
  { id: 3285, name: 'Мотоциклы и другая мототехника' },
  { id: 3286, name: 'Водный транспорт' },
  { id: 3287, name: 'Автопроизводитель' },
  { id: 3288, name: 'Автомойка' },
  { id: 3117, name: 'Автошкола' },
  { id: 3118, name: 'Детский сад' },
  { id: 3119, name: 'Гимназия' },
  { id: 3120, name: 'Колледж' },
  { id: 3121, name: 'Лицей' },
  { id: 3122, name: 'Техникум' },
  { id: 3123, name: 'Университет' },
  { id: 3124, name: 'Школа' },
  { id: 3125, name: 'Институт' },
  { id: 3126, name: 'Обучающие курсы' },
  { id: 3276, name: 'Дополнительное образование' },
  { id: 3275, name: 'Тренинг, семинар' },
  { id: 3127, name: 'Танцевальная школа' },
]

const MusicScreen = () => {
  const router = useRouter()

  return (
    <>
      <Header>Выбрать музыку</Header>
      <Search placeholder="Поиск" onChange={() => console.log()} after={null} />
      {thematics.length > 0 && (
        <List>
          {thematics.map((thematic) => {
            return (
              <SimpleCell
                onClick={() => router.goForward(PanelId.AudioEditing, { useBackgroundMusic: true })}
                before={<Avatar mode="image" src={mocart}/>}
                description="Амадей Моцарт"
                after={<Icon24MoreHorizontal fill="var(--accent)" />}
              >
                Какая-то душ(ев)ная песня
              </SimpleCell>
            )
          })}
        </List>
      )}
    </>
  )
}

export default MusicScreen
